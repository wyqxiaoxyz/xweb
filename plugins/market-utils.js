import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$getStocksInfo = async (stockhash_id) => {
  const res = stockhash_id.split('-')
  const stocksMarketType = res[0]
  const stockId = res[1]
  return await axios
    .get(`/api/market/stocks-info-sum/${stocksMarketType}/${stockId}`)
    .catch((error) => {
      return error
    })
    .then((response) => {
      if (response.data) return response.data
      return {
        status: 'error',
        content: 'No data found from response'
      }
    })
}

Vue.prototype.$getMultiStocksInfo = async (stockhash_id) => {
  const res = stockhash_id.split('-')
  const stocksMarketType = res[0]
  return await axios
    .get(`/api/market/stocks-info-sum/${stocksMarketType}`)
    .catch((error) => {
      return error
    })
    .then((response) => {
      if (response.data) return response.data
      return {
        status: 'error',
        content: 'No data found from response'
      }
    })
}
