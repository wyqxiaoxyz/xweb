import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$getArticle = async (articlehash_id) => {
  return await axios
    .get(`/api/articles/${articlehash_id}`)
    .catch((error) => {
      return error
    })
    .then((response) => {
      if (response.data) return response.data
      return {
        status: 'error',
        content: 'No data found from response'
      }
    })
}

Vue.prototype.$getMultiArticles = async () => {
  return await axios
    .get('/api/articles/')
    .catch((error) => {
      return error
    })
    .then((response) => {
      if (response.data) return response.data
      return {
        status: 'error',
        content: 'No data found from response'
      }
    })
}
